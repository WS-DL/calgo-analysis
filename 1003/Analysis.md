# ALG 1003 - Mongoose, a Graph Coarsening and Partitioning Library

## Metadata
``` yaml
file: 1003.zip
ref: TOMS 46,1 (Apr 2020) Article: 7
for: Mongoose, a Graph Coarsening and Partitioning Library
by: Davis, Timothy A., Hager, William W., Kolodziej, Scott P. and Yeralan, S. Nuri
size: 730 kB
```

<br /><br /><br />

## Archive Analysis

File and directory contents for this archive can be viewed at the links below:

## [Archive Manifest](files/1003-contents.txt)

## [File Tree](files/1003-tree.txt)

<br /><br /><br />

## Code Analysis

![](screens/1003-github.webp)

The project source code is hosted on [GitHub](https://github.com/ScottKolo/Mongoose) but neither the paper nor the ACM page include a reference or mention to it. At the time of writing has 10 stars and 5 forks and the current release version matches the ACM Digital Library archive. While there has not been a major release of the software since the [CoSAI](https://dl.acm.org/doi/abs/10.1145/3337792) induction, there have been a number of commits since that time.

![](screens/1003-git-commits.webp)

These include a fix data type fix for 64-bit calculations but most of the recent commits have been for the project's testing tools rather than algorithmic of project code changes.

<br /><br /><br />

## Web Analysis

### Google
Google's search results for this Algorithm are a bit varied. The top result is a direct PDF link but it is linking to the preprint version of the paper. The second and third links direct readers to the ACM and GitHub page as well as relevant SemanticScholar and ResearchGate links found further down. (Not pictures in the Google screenshot were the additional image results and figures related to the search query).

![](screens/1003-google_results.webp)

<br />

### Bing
Bing did a much better job of displaying relevant search results for this Algorithm. The [ACM Digital Library](https://dl.acm.org/) is both the top and the second result, followed by the project's GitHub page. There are two direct PDF results on Bing's front page, one for each published version of the paper. There also were a larger-that-normal amount of government and educational links in the search results for this Algorithm.

![](screens/1003-bing_results.webp)

<br /><br /><br />

## Academic Analysis

Algorithm 1003 did not have an arXiv or Papers With Code entry available at the time of searching. Papers With Code did have [another paper using the moniker Mongoose](https://paperswithcode.com/paper/mongoose-a-learnable-lsh-framework-for).

![](screens/1003-papers_with_code.webp)

<br />

### ACM Digital Library

The ACM Digital Library page contains a [zip archive](https://dl.acm.org/doi/abs/10.1145/3337792#sec_supp) containing the relevant source code for version Algorithm 1003 (v2.0.4), though this archive is not automatically synced with new project udpates.

![](screens/1003-acm_dl.webp)


### Google Scholar

Google Scholar returned a good amount of direct references and references of other papers citing our target paper. The top result index is to the [ACM Digital Library](https://dl.acm.org/).

![](screens/1003-google_scholar.webp)

<br />

### Internet Archive Scholar

The Internet Archive Scholar did not have a direct reference to the Algorithm 1003 paper, but it did have a refence to another paper, translated to "Study and development of approaches to evaluate clustering results in graphs", citing the Mongoose paper!

![](screens/1003-ia_scholar.webp)

<br />

### National Science Foundation
An entry for this publication could also be found in the [NSF Public Access Repository](https://par.nsf.gov/biblio/10170049-algorithm-mongoose-graph-coarsening-partitioning-library)

![](screens/1003-nsf.webp)
