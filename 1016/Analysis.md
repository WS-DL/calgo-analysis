# ALG 1016 - {PyMGRIT}: A Python Package for the Parallel-in-time Method {MGRIT}

## Metadata
``` yaml
file: 1016.zip
ref: TOMS 47,2 (Apr 2021) Article: 19
for: {PyMGRIT}: A Python Package for the Parallel-in-time Method {MGRIT}
by: Hahne, Jens, Friedhoff, Stephanie and Bolten, Matthias
size: 13249 kB
```

## Archive Analysis

A detailed list of file contents for this archive can be found [here](files/1016-contents.txt), and additionally in a more visually friendly format [here](/files/1016-tree.txt).

![](screens/1.webp)

<br />

![](screens/2.webp)

<br />

![](screens/3.webp)

<br />

![](screens/4.webp)

<br />

![](screens/5.webp)

<br />

![](screens/6.webp)

<br />

![](screens/7.webp)

<br />

![](screens/8.webp)

<br />

![](screens/9.webp)

<br />

![](screens/10.webp)

<br />

![](screens/11.webp)

<br />

![](screens/12.webp)

<br />

![](screens/13.webp)

<br />

![](screens/14.webp)