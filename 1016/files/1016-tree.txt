1016/
├── 1016.head
├── build_docs.sh*
├── docs/
│   ├── make.bat
│   ├── Makefile
│   └── source/
│       ├── applications/
│       │   ├── advection.rst
│       │   ├── arenstorf_orbit.rst
│       │   ├── brusselator.rst
│       │   ├── dahlquist.rst
│       │   └── heat_equation.rst
│       ├── conf.py
│       ├── coupling/
│       │   ├── firedrake.rst
│       │   └── petsc.rst
│       ├── figures/
│       │   ├── advection.png
│       │   ├── arenstorf_orbit.png
│       │   ├── brusselator.png
│       │   ├── dahlquist.png
│       │   ├── strong_scaling.png
│       │   └── tutorial.png
│       ├── help/
│       │   └── faq.rst
│       ├── index.rst
│       ├── modules.rst
│       ├── pymgrit.advection.rst
│       ├── pymgrit.arenstorf_orbit.rst
│       ├── pymgrit.brusselator.rst
│       ├── pymgrit.core.rst
│       ├── pymgrit.dahlquist.rst
│       ├── pymgrit.heat.rst
│       ├── pymgrit.induction_machine.rst
│       ├── pymgrit.rst
│       ├── _static/
│       │   └── css/
│       │       └── custom.css
│       └── usage/
│           ├── advanced.rst
│           ├── examples.rst
│           ├── installation.rst
│           ├── parallelism.rst
│           ├── quickstart.rst
│           └── tutorial.rst
├── examples/
│   ├── example_advection.py
│   ├── example_arenstorf.py
│   ├── example_brusselator.py
│   ├── example_convergence_criterion.py
│   ├── example_dahlquist.py
│   ├── example_heat_1d_bdf2.py
│   ├── example_heat_1d.py
│   ├── example_heat_2d.py
│   ├── example_multilevel_structure.py
│   ├── example_output_fcn.py
│   ├── example_output_fcn_serial.py
│   ├── example_parameters.py
│   ├── example_spatial_coarsening.py
│   ├── example_time_integrators.py
│   ├── firedrake/
│   │   └── example_firedrake_diffusion_2d.py
│   ├── induction_machine/
│   │   └── induction_machine.py
│   ├── petsc4py/
│   │   └── example_petsc_heat_2d.py
│   ├── results/
│   │   └── .gitignore
│   └── toms/
│       ├── example_1_heat1d.py
│       └── example_3_petsc.py
├── LICENSE.rst
├── README.rst
├── setup.cfg
├── setup.py
├── src/
│   └── pymgrit/
│       ├── advection/
│       │   ├── advection_1d.py
│       │   └── __init__.py
│       ├── arenstorf_orbit/
│       │   ├── arenstorf_orbit.py
│       │   └── __init__.py
│       ├── brusselator/
│       │   ├── brusselator.py
│       │   └── __init__.py
│       ├── core/
│       │   ├── application.py
│       │   ├── grid_transfer_copy.py
│       │   ├── grid_transfer.py
│       │   ├── __init__.py
│       │   ├── mgrit.py
│       │   ├── mgrit_with_plots.py
│       │   ├── simple_setup_problem.py
│       │   ├── split.py
│       │   └── vector.py
│       ├── dahlquist/
│       │   ├── dahlquist.py
│       │   └── __init__.py
│       ├── heat/
│       │   ├── heat_1d_2pts_bdf1.py
│       │   ├── heat_1d_2pts_bdf2.py
│       │   ├── heat_1d.py
│       │   ├── heat_2d.py
│       │   ├── __init__.py
│       │   └── vector_heat_1d_2pts.py
│       ├── induction_machine/
│       │   ├── getdp/
│       │   │   └── .gitignore
│       │   ├── grid_transfer_machine.py
│       │   ├── helper.py
│       │   ├── im_3kW/
│       │   │   ├── BH.pro
│       │   │   ├── im_3kW_17k.msh
│       │   │   ├── im_3kW_17k.pre
│       │   │   ├── im_3kW_4k.msh
│       │   │   ├── im_3kW_4k.pre
│       │   │   ├── im_3kW_69k.msh
│       │   │   ├── im_3kW_69k.pre
│       │   │   ├── im_3kW_circuit.pro
│       │   │   ├── im_3kW_data.geo
│       │   │   ├── im_3kW.db
│       │   │   ├── im_3kW.geo
│       │   │   ├── im_3kW_orig.msh
│       │   │   ├── im_3kW_orig.pro
│       │   │   ├── im_3kW.pro
│       │   │   ├── im_3kW_rotor.geo
│       │   │   ├── im_3kW_stator.geo
│       │   │   ├── machine_magstadyn_a_orig.pro
│       │   │   └── machine_magstadyn_a.pro
│       │   ├── induction_machine.py
│       │   ├── __init__.py
│       │   ├── mgrit_machine_conv_jl.py
│       │   ├── mgrit_machine.py
│       │   └── vector_machine.py
│       └── __init__.py
├── tests/
│   ├── advection/
│   │   ├── __init__.py
│   │   └── test_advection_1d.py
│   ├── arenstorf_orbit/
│   │   ├── __init__.py
│   │   └── test_arenstorf_orbit.py
│   ├── brusselator/
│   │   ├── __init__.py
│   │   └── test_brusselator.py
│   ├── core/
│   │   ├── __init__.py
│   │   ├── test_application.py
│   │   ├── test_grid_transfer_copy.py
│   │   ├── test_mgrit.py
│   │   └── test_simple_setup_problem.py
│   ├── dahlquist/
│   │   ├── __init__.py
│   │   └── test_dahlquist.py
│   ├── .gitignore
│   ├── heat/
│   │   ├── __init__.py
│   │   ├── test_heat_1d_2pts_bdf1.py
│   │   ├── test_heat_1d_2pts_bdf2.py
│   │   ├── test_heat_1d.py
│   │   ├── test_heat_2d.py
│   │   └── test_vector_heat_1d_2pts.py
│   ├── induction_machine/
│   │   ├── im_3kW_17k.msh
│   │   ├── im_3kW_17k.pre
│   │   ├── im_3kW_4k.msh
│   │   ├── im_3kW_4k.pre
│   │   ├── im_3kW.res
│   │   ├── im_3kW_test_prefile.pre
│   │   ├── __init__.py
│   │   ├── resJL.dat
│   │   ├── test_grid_transfer_machine.py
│   │   ├── test_helper.py
│   │   └── test_vector_machine.py
│   └── mpi/
│       ├── __init__.py
│       ├── mpi.py*
│       ├── mpi.sh*
│       ├── mpi_testsets.sh*
│       ├── procs_without_points.py
│       ├── results/
│       │   ├── brusselator
│       │   ├── convergence_criterion
│       │   ├── dahlquist
│       │   ├── heat_1d
│       │   ├── heat_2d
│       │   ├── multilevel_structure
│       │   ├── parameters
│       │   ├── procs_without_points
│       │   ├── spatial_coarsening
│       │   ├── time_integrators
│       │   └── varying_coarsening
│       ├── test_examples.sh*
│       └── varying_coarsening.py
└── tox.ini

36 directories, 160 files
