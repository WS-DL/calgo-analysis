# ALG 613 - Minimum Spanning Tree for Moderate Integer Weights

## Metadata
``` yaml
file: 613.gz
keywords: minimum spanning tree, shortest connection network
gams: G2d2
title: MSTPAC
for: minimum spanning tree for moderate integer weights in a connected undirected graph represented in a forward star data structure
by: R.E. Haymond, J.P. Jarvis, and D.R. Shier
ref: ACM TOMS 10 (1984) 108-111
```
<br /><br /><br />

## Archive Information
The `613.gz` is itself a single, compressed binary file without file type extension. The ([included](files/613.f)) uncompressed file contains 223 lines of text representing metadata and the algorithm itself.

<br /><br /><br />

## Code Analysis

Algorithm 613 was added to the CoSAI archive in 1984 and as such does not have a GitHub repository. The code itself is contained within a single FORTRAN file. [Further analysis has been conducted](https://leeds-faculty.colorado.edu/glover/221%20-%20An_in-depth_empirical_investigation_of_non-greedy_approaches_to_the_MST.pdf) on MST using the findings and algorithm described by Haymond et al. examining greedy vs. non-greedy approaches to minimum spanning tree problems. It was also utilized for an optimization edge case in the paper [Fast Prediction on a Tree](https://proceedings.neurips.cc/paper/2008/hash/677e09724f0e2df9b6c000b75b5da10d-Abstract.html), published to Advances in Neural Information Processing Systems 21, in 2008.

The individual FORTRAN source file is available directly from the ACM Digital Library page's [Supplemental Material](https://dl.acm.org/doi/10.1145/356068.356077#sec-supp) section.

<br /><br /><br />

## Web Results
A cursory search of the title does not yeild promising results; there are a handful of similar acronyms of varied search popularity and none of the first page results are related to the transaction target.

![](screens/613-google-1.webp)

<br />

Including the authors to the search yeilded much better results with the ACM-DL being the first result.

![](screens/613-google-2.webp)

<br />

A search of the document reference yeilded the best results 8 relevant search hits as well as as handful of relevant image results with the 1st, 2nd, 4th, and 9th being [dl.acm.org] links.

![](screens/613-google-3.webp)

![](screens/613-google-4.webp)

<br /><br /><br />

# Web Links

![](screens/613-dl.acm.org.webp)

[dl.acm.org/doi/10.1145/356068.356077](https://dl.acm.org/doi/10.1145/356068.356077)

<br />

7 citations on Google Scholar

![](screens/613-google-scholar.webp)