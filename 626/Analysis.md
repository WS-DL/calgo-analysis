# ALG 626 - TRICP

## Metadata
``` yaml
file: 626.gz
keywords: contour plotting, triangular mesh, FEM
gams: Q
title: TRICP
for: computing contours of a function defined by a set of irregularly distributed data points in the plane.
by: A. Preusser
ref: ACM TOMS 10 (1984) 178-189
```

<br /><br /><br />

## Archive Information
The `626.gz` archive contains a single, compressed binary file extracting to 1899 lines of text representing metadata and algorithmic code. The [raw file has been included](files/626.f) in the repository.

<br /><br /><br />

## Code Analysis

The code for the TRICP algorithm is available on the ACM Digital Library page, under the [Supplemental Materials](https://dl.acm.org/doi/abs/10.1145/2701.2772#sec-supp) section. The TRICP mathematical transaction submission was added in 1984, long before GitHub was around and the project does not list any other publically available source repository or home page beyond that listed on the ACM Digital Library page.

<br /><br /><br />

## Web Analysis
Google SERP for `TRICP` alone turned up poor results overall, though it did manage to hit the direct ACM link as the third result. Using the `author` + `for` description from the CALGO page yeilded similar results, though Google returned the dl.acm.org as the second result.

![](screens/626-google-1.webp)

![](screens/626-google-2.webp)

Using the title from the ACM page was the best result with two dl.acm.org links, three relevant pages (although only two of quality, shown below), and a hit as the 3rd result in Google Images.

![](screens/626-google-3.webp)

![](screens/626-google-4.webp)

Google Scholar listed 24 citations for this publication

![](screens/626-google-scholar.webp)

# Web Links

[dl.acm.org/doi/10.1145/2701.2772](https://dl.acm.org/doi/10.1145/2701.2772)

![](screens/626-acm.webp)


[semanticscholar.org/paper/Algorithm-626%3A-TRICP%3A-a-contour-plot-program-meshes-Preusser/f1779c1f4fa3e91ff9cfbcc3df6d3104bbb4d581](https://www.semanticscholar.org/paper/Algorithm-626%3A-TRICP%3A-a-contour-plot-program-meshes-Preusser/f1779c1f4fa3e91ff9cfbcc3df6d3104bbb4d581)

![](screens/626-semantic-scholar.webp)

[people.sc.fsu.edu/~jburkardt/f77_src/toms626/toms626.html](https://people.sc.fsu.edu/~jburkardt/f77_src/toms626/toms626.html)

![](screens/626-jburkardt.webp)