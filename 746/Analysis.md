# ALG 746 - pcomp

## Metadata
``` yaml
file: 746.gz
ref: TOMS 21,3 (Sep 1995) 233
alg: pcomp
for: fortran code for automatic differentiation
by: M. Dobmann, M. Liepelt and K. Schittkowski
size: 284 kB
```

<br /><br /><br />

## Archive Information
Single, binary shell archive file (the full, extracted version can be viewed [here](files/746)) which requires further steps to execute properly (as described near the top of the file).

``` sh
#! /bin/sh
# This is a shell archive, meaning:
# 1. Remove everything above the #! /bin/sh line.
# 2. Save the resulting text in a file.
# 3. Execute the file with /bin/sh (not csh) to create the files:
```

The binary file itself contains 29760 lines of text.


<br /><br /><br />

## Code Analysis


<br /><br /><br />

## Web Results
The Google SERP for the pcomp entry yielded by far the most results of the selected entries for review, not only in volume but also relevancy. The dl.acm.org link was the second result hit with the 24th being a CALGO mirror, [NetLib](https://netlib.org/toms/).

![](screens/746-google-1.webp)

![](screens/746-google-2.webp)

<br />

The first result is an updated version (v5.3, released September 1996) of the PCOMP algorithm compared to the CALGO version.

![](screens/746-v5.3.webp)


The result of this is that subsequent results were linking to and discussing different versions of the algorithm.

This can be seen by the third (StudyLib) and sixth (CireSeerX) results linking to the 1996 5.3 version...

![](screens/746-studylib.webp)

![](screens/746-citeseer.webp)

... the fifth (Scinapse) referencing to the 1995 CALGO version...

![](screens/746-scinapse.webp)

... and the fourth (ResearchGate) which links to the CALGO version, despite displaying an even older 1994 version on it's own page.

![](screens/746-researchgate.webp)

<br />

Google Scholar shows multiple revisions of the publication, each listing multiple versions. When investigating this though, each revision had only the version listed in the search results page.

![](screens/746-google-scholar.webp)

<br /><br /><br />

# Web Links
[dl.acm.org/doi/10.1145/210089.210091](https://dl.acm.org/doi/10.1145/210089.210091)

[schittkowski.de (direct PDF link)](https://www.schittkowski.de/downloads/numerical/PCOMPDOC.pdf) (1st result)

[scinapse.io/papers/2037341904](https://www.scinapse.io/papers/2037341904)

[researchgate.net/publication/234790639_Algorithm_746_PCOMP_A_Fortran_Code_for_Automatic_Differentiation](https://www.researchgate.net/publication/234790639_Algorithm_746_PCOMP_A_Fortran_Code_for_Automatic_Differentiation)

[citeseerx.ist.psu.edu/viewdoc/citations;jsessionid=89345DFC3A534AB155F5394AD2FA11E0?doi=10.1.1.908.9438](https://citeseerx.ist.psu.edu/viewdoc/citations;jsessionid=89345DFC3A534AB155F5394AD2FA11E0?doi=10.1.1.908.9438)

[Google Books](https://www.google.com/books/edition/PCOMP/uH5HcgAACAAJ?hl=en)

![](screens/746-google-books.webp)