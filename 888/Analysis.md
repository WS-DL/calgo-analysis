# ALG 888 - Spherical Harmonic Transform Algorithms

## Metadata
``` yaml
file: 888.zip
ref: TOMS 35,3 (Oct 2008) Article: 23
for: Spherical Harmonic Transform Algorithms
by: J. B. Drake, P. Worley and E. {D'Azevedo}
size: 528 kB
```

<br />
<br />

## Archive Information
A detailed list of file contents for this archive can be found [here](files/888-contents.txt), and additionally in a more visually friendly format [here](files/888-tree.txt).

<br /><br /><br />

## Web
Initial Google result shows the ACM Digital Libary in second place, behind the top CiteSeerX result and suggested scholarly publications.

![](screens/888-google-1.webp)

<br />

Cited 25 times on Google Scholar.
There are 8 listed versions, though 3 are citations. The 5 remaining 'versions' actually identical to the CALGO version.

![](screens/888-google-scholar.webp)

<br /><br /><br />

## Code Analysis
There was no code repository available for Algorithm 888 and thus could not be included as a reference. The ACM Digital Library does keep the archived source code [publically available](https://dl.acm.org/doi/10.1145/1391989.1404581#sec-supp).

![](screens/888-acm-dl-download.webp)

<br /><br /><br />

## Web Links
[dl.acm.org/doi/10.1145/1391989.1404581](https://dl.acm.org/doi/10.1145/1391989.1404581)

<br />

The ACM Digital Libary page for Algorithm 888 does not make the paper available for public access which is most likely the reason the direct CiteSeerX PDF result is coming out ahead on the Google SERP.

![](screens/888-acm-dl.webp)

<br />

[CiteSeerX direct PDF link](http://citeseerx.ist.psu.edu/viewdoc/download?doi=10.1.1.141.1535&rep=rep1&type=pdf#:~:text=The%20spherical%20harmonic%20transform%20is%20used%20to%20project%20grid%20point,information%20in%20a%20synthesis%20step.)

![](screens/888-citeseerx.webp)
