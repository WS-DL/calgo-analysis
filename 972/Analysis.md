# ALG 972 - JMarkov: An Integrated Framework for Markov Chain Modeling

## Metadata
``` yaml
file: 972.zip
ref: TOMS 43,3 (Jan 2017) Article: 29
for: JMarkov: An Integrated Framework for Markov Chain Modeling
by: Juan F. P{\'e}rez, Daniel F. Silva, Julio C. G\'{o}ez, Andr{\'e}s Sarmiento, Andr{\'e}s Sarmiento-Romero, Raha Akhavan-Tabatabaei and Germ\'{a}n Ria\~{n}o
size: 26928 kB
```

<br /><br /><br />

## Archive Information
The JMarkov archive contains 96 directories and 1036 files at just under 27MB. The top-level contains a .head file, a documentation directory containing PDF user manuals and various LaTeX (.tex) files. The main bulk of the archive is the Java code and artifacts at 15.5MB and the combined documentation coming in at about 8.5MB.

A detailed list of file contents for this archive can be found [here](files/972-contents.txt), and additionally in a more visually friendly format [here](files/972-tree.txt).

<br /><br /><br />

## Code Analysis
The jMarkov project does have source code available in a public [GitHub repository](https://github.com/coin-or/jMarkov) and is referenced in the ACM Digital Library page but the specified homepage for the project, `project.coin-or.org`, now redirects to the project's GitHub repository rather than a dedicated homepage. The repository is listed at number #33 in the references bibliography.

![](screens/972-acm_gh_ref.webp)

<br />

The project's GitHub repository has more recent updates than that of the CALGO archive, with the most recent commit being added in 2019. The project structure has been updated in this time as well, which can bee seen in the highlighted area to the left of the page. Many project directories (such as `lib`, `javadoc`, `.coin-or`, and `test`) have been added to the repository and some have been refactored, such as the `doc` directory which moved the supplementary LaTeX code and source files to a nested `src` directory.

![](screens/972-github.webp)

The project has no listed releases available to download but a v1.0 release from 2017 can be found in the repository's Tags page. This could be a case of improper tagging causing the release to not be listed properly for download but regardless the only release available is outdated compared to the repository source code.

![](screens/972-github-releases.webp)

![](screens/972-github-tags.webp)

<br /><br /><br />

## Web Results
The JMarkov archive was relatively new, being added in 2017, which helped with the web search and turned up a health amount of front-page results. The first result directly links to the ACM-DL page with 3rd, 4th, and 5th results being academically linked results, the 6th result being the project's GitHub repository, and the 7th result linking to a previous publication version on the personal website of one of the authors. ACM-DL and project home page links are highlighted in red, relevant links are highlighted in green, and unrelated links in yellow.

![](screens/972-google-1.webp)

<br />

The first two Google Image results were direct to the project homepage and ACM-DL entry as well.

![](screens/972-google-imgs.webp)

<br />

Google Scholar results turned up three relevant results at 1st, 2nd, and 3rd indicies. Each Google Scholar result lists multiple versions though upon checking they all display only the entry listed on the results page.

![](screens/972-google-scholar.webp)

<br /><br /><br />

## Web Links
### ACM-DL
[dl.acm.org/doi/10.1145/3009968/](https://dl.acm.org/doi/10.1145/3009968)

The ACM-DL page for the archive has closed access to the paper itself, but the archived source material is publically available for download.

![](screens/972-acm-dl.webp)

<br />

![](screens/972-acm_archive_dl.webp)

There are numerous references listed with three linking to the project homepage itself. The references for these entries are quite far down the list though, at 33rd, 55th, and 56th. There is also a self-referencing ACM reference at number 52.

![](screens/972-acm-dl-ref-2.webp)

<br /><br /><br />

## Other Web Links
[Julio C Goez](http://www.juliocgoez.com/sites/default/files/jMarkov.pdf)

Julio Goez, one of the authors, hosts a version of the paper on this website but it appears to be an older version dating to 2015. The top front-matter of the document above the Introduction and the listing of 'Algorithm XXX' lends to the notion that this copy is a draft not completely intended for publication.

![](screens/972-julio-pdf.webp)

<br />

### ResearchGate
[researchgate.net/publication/312207464_Algorithm_972_jMarkov_An_Integrated_Framework_for_Markov_Chain_Modeling](https://www.researchgate.net/publication/312207464_Algorithm_972_jMarkov_An_Integrated_Framework_for_Markov_Chain_Modeling)

ResearchGate also has holds the publication behind a paywall.

![](screens/972-researchgate.webp)

<br />

### Other
jMarkov recieved a handful of attention from international institutions, such as the [Universidad del Rosario](https://repository.urosario.edu.co/handle/10336/23121))

![](screens/972-crai.webp)