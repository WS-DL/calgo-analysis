# ALG 990 - Efficient Atlasing and Search of Configuration Spaces of Point-Sets Constrained by Distance Intervals

## Metadata
``` yaml
file: 990.zip
ref: TOMS 44,4 (Aug 2018) Article: 48
for: Efficient Atlasing and Search of Configuration Spaces of Point-Sets Constrained by Distance Intervals
by: Aysegul Ozkan, Rahul Prabhu, Troy Baker, James Pence, Jorg Peters and Meera Sitharam
size: 7944 kB
```

<br /><br /><br />

## Archive Information
A detailed list of file contents for this archive can be found [here](files/990-contents.txt), and additionally in a more visually friendly format [here](files/990-tree.txt).

<br /><br /><br />

## Code Analysis

The [ACM Digital Libary page for Algorithm 990](https://dl.acm.org/doi/10.1145/3204472#sec-supp) and the [Collected Algorithms](https://calgo.acm.org/) provide the same archive version but the project's official [BitBucket repository](https://bitbucket.org/geoplexity/easal/src/master/) contains a slightly different directory structure.

![](screens/990-bitbucket.webp)

<br />

The documentation for the project felt as if it could be clearer in defining which version the reader is looking at. There are two versions of the development manual, one TOMS version and one "Complete" version. The documentation in the BitBucket has the same title and date with different content, while the ACM version has still different content and an more recent date.

![](screens/990-dev_manual.webp)

<br /><br /><br />


## Web Analysis
### Google
Google's search results shows a healthly amount of results for this algorithm. Google's ranking system listed two ResearchGate results but listed the preprint version of the paper as a primary search result and the final version as a nested, related result.

![](screens/990-google_results.webp)

<br />

### Bing
Microsoft Bing's search results felt more focused than Google but also more repetitive, again listing both arXiv versions in addition to two versions of the publication's relevant ACM-DL page. Bing's ranking system also placed the ResearchGate preprint reference higher in the search results than the final version, though it did not co-relate the two. Bing did list a Papers With Code reference in its top ten results which was not the case with Google.

![](screens/990-bing_results.webp)

<br /><br /><br />


## Academic Analysis

### Google Scholar
Google Scholar lists the [ACM Digital Libary page](https://dl.acm.org/doi/10.1145/3204472) as the top search result.

![](screens/990-google_scholar.webp)

<br />

### ACM Digital Libary
The [ACM Digital Libary](https://dl.acm.org/doi/10.1145/3204472) contains an downloadable archive of the project's code but it's slightly different content structure in relation to the project's GitHub repository (see "Code Analysis section" above).

![](screens/990-acm_dl.webp)

There is a reference to the project repository on the ACM DL page but the reference is quite far down the list at #43.

![](screens/990-acm_dl_bitbucket_ref.webp)

<br />

### arXiv
arXiv has a repository link for the Algorithm 990 project, though the "Demos" tab does not contain any helpful visuals or code being executed. The arXiv page does not itself link to the appropriate Papers With Code page, although there is an existing PWC entry for Algorithm 990 which links back to arXiv.

![](screens/990-arxiv.webp)

<br />

### Papers With Code
Papers With Code contains [an entry for this publication](https://cs.paperswithcode.com/paper/efficient-atlasing-and-search-of) with arXiv.org links for the abstract and the direct PDF.

![](screens/990-papers_with_code.webp)

<br />

### Semantic Scholar
The preprint version of this algorithm has a reference on Semantic Scholar but the "View Paper" link does not link to [arXiv entry](https://arxiv.org/abs/1805.07450) as most results or to the [ACM-DL page](https://dl.acm.org/doi/10.1145/3204472) directly. Instead Semantic Scholar chooses to [WikiData](https://wikidata.org/entity/Q113310081), which then points the reader to the ACM-DL entry via a [DOI link](https://doi.org/10.1145/3204472).

![](screens/990-semantic_scholar.webp)

![](screens/990-wikidata.webp)

<br />

### DeepAI
[DeepAI](https://deepai.org/publication/efficient-atlasing-and-search-of-configuration-spaces-of-point-sets-constrained-by-distance-intervals) came up in both Google and Bing search results but feels a bit more proprietary than scholarly. The links to author publications are circular, referencing back to DeepAI pages for each auther instead of more widely-used community websites such as Google Scholar or ORCiD.

![](screens/990-deepai.webp)

<br />

### Internet Archive Scholar
Unfortunately there were no relevant results for either version of the publication on Internet Archive's Scholar search engine, though it is still in beta!

![](screens/990-ia_scholar.webp)