# Selected analysis of the [ACM-DL](https://dl.acm.org/) [Collected Algorithms](https://calgo.acm.org/) repository

## Current Algorithms Analyzed
Archive | Description
--- | ---
613 | [Minimum Spanning Tree for Moderate Integer Weights](613/Analysis.md)
626 | [GROW](626/Analysis.md)
746 | [pcomp](746/Analysis.md)
888 | [Spherical Harmonic Transform Algorithms](888/Analysis.md)
972 | [JMarkov: An Integrated Framework for Markov Chain Modeling](972/Analysis.md)
990 | [Efficient Atlasing and Search of Configuration Spaces of Point-Sets Constrained by Distance Intervals](990/Analysis.md)
1003 | [Mongoose, a Graph Coarsening and Partitioning Library](1003/Analysis.md)
1016 | [PyMGRIT: A Python Package for the Parallel-in-time Method MGRIT](1016/Analysis.md)
